ActiveAdmin.register Photo do
  config.filters = false
  
  form partial: 'form'

  permit_params :title, :date, :photo_album_id, {files:[]}, :description

  menu label: 'Фотографии', if: proc { current_admin_user.photograph? }

  index do
    selectable_column
    column :title
    column :photo_album
    column :date
    column :description
    actions
  end

  show do
    attributes_table do
      row :title
      row :photo_album
      row :files do
        photo.files.each do |file|
          div do
            image_tag file.url(:small), width: '150px'
          end
        end
      end
      row :date
      row :description
    end
  end

  member_action :remove_photo do
    @photo = Photo.find(params[:id])
    images = @photo.files
    images.delete_at(params[:index].to_i)
    if images.count == 0
      @photo.destroy
      redirect_to admin_photos_path
    else
      @photo.files = images
      redirect_to edit_admin_photo_path(@photo) if @photo.save
    end
  end

  controller do
    def update
      @photo = Photo.find(params[:id])
      add_more_images(params[:photo][:files])
      params[:photo].delete(:files)
      super
    end

    def index
      @photos = current_admin_user.photo_album.photos.page(params[:page]).per(10)
    end

    private

      def add_more_images(files)
        images = @photo.files 
        images += files 
        @photo.files = images
      end
  end
end
