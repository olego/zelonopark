ActiveAdmin.register PhotoAlbum do
  config.filters = false
  
  menu if: proc{ current_admin_user.admin? }

  permit_params :title, :start_date, :finish_date, :description, :logo
  
  index do
    selectable_column
    column :title
    column :logo
    column :start_date
    column :finish_date
    actions
  end

  form do |f|
    f.inputs 'Общая информация' do
      f.input :title
      f.input :logo
      f.input :start_date, as: :datepicker
      f.input :finish_date, as: :datepicker
      f.input :description
    end
    f.actions
  end
end
