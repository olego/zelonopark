$(window).load(function() {
    var $modalCloseButton = $('.nodalwin__closer');
    var $modalOpenButton = $('.galery__preview-photo__action-slider');
    var $modalwinBackground = $('.modalwin__background');
    var $modalwin = $('.modalwin');

    var hideModalWin = function () {
        $modalwin.removeClass('modalwin--shown');
    }

    var showModalWin = function() {
        $modalwin.addClass('modalwin--shown');
    }

    $(window).on('keydown', function(event) {
        if (event.keyCode === 27) {
            hideModalWin();
        }
    });

    $modalCloseButton.on('click', hideModalWin);

    $modalwinBackground.on('click', hideModalWin);

    $modalOpenButton.on('click', function(event) {
        var photoIndex = parseInt($(this).attr("data-index"));

        showModalWin();

        setTimeout(function() {
            var $fotorama = $('.fotorama').data('fotorama');

            $fotorama.show(photoIndex);
        }, 300);

        var sliderHeader = $(this).closest('.galery__preview-item').find('.galery__preview-header').text();
        var sliderDescription = $(this).closest('.galery__preview-item').find('.galery__preview-description').text();

        $('.modalwin__slider-aside__header').text(sliderHeader);
        $('.modalwin__slider-aside__description').text(sliderDescription);
    });

    if (document.getElementById('grid')) {
        new AnimOnScroll( document.getElementById('grid'), {
            minDuration : 0.4,
            maxDuration : 0.7,
            viewportFactor : 0
        } );
    }
});
