$(document).ready(function() {
    var $photoListNavButton = $('.galery__photos__navigation-button');
    var activeButtonClass = 'galery__photos__navigation-button--active'

    var photosListToggler = function(event) {
        $photoListNavButton.removeClass(activeButtonClass);
        $(this).addClass(activeButtonClass);
    }

    $photoListNavButton.on('click', photosListToggler);
});
