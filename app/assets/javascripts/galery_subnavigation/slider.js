$(document).ready(function() {
    var $navButton = $('.modalwin__body__nav-button');

    var fillSlideInfo = function() {
        var $fotorama = $('.fotorama').data('fotorama');
        var currentIndex = $fotorama.activeIndex;

        var sliderHeader = $('.galery__preview-photo__action-slider[data-index="' + currentIndex + '"]').closest('.galery__preview-item').find('.galery__preview-header').text();
        var sliderDescription = $('.galery__preview-photo__action-slider[data-index="' + currentIndex + '"]').closest('.galery__preview-item').find('.galery__preview-description').text();

        $('.modalwin__slider-aside__header').text(sliderHeader);
        $('.modalwin__slider-aside__description').text(sliderDescription);
    }

    $navButton.on('click', function(event) {
        var $fotorama = $('.fotorama').data('fotorama');
        var direction = $(this).attr('data-direction');

        $fotorama.show(direction);

        fillSlideInfo();
    });

    $('.fotorama').on('fotorama:showend', function (e, fotorama, extra) {
      fillSlideInfo();
    });
});
