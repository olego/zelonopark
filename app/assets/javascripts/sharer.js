Share = {
    vkontakte: function(ptitle, text) {
        redirect_url = $('.fotorama').data('fotorama')['activeFrame']['img']
        url  = 'http://vkontakte.ru/share.php?url=http://z-fest.ru';
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image=http://z-fest.ru'  + redirect_url;
        url += '&noparse=true';
        Share.popup(url);
    },

    facebook: function(purl, ptitle, text) {
        redirect_url = $('.fotorama').data('fotorama')['activeFrame']['img']
        url = "https://www.facebook.com/sharer.php?u=http://z-fest.ru&t=Zelenopark"
        url += '&picture=http://z-fest.ru' + redirect_url;
        url += '&description=Zelenopark'
        Share.popup(url);
    },
    

    me: function(el){
        console.log(el.href);                
        Share.popup(el.href);
        return false;                
    },

    downloadImage: function(link){
        el = $('#download-link')
        image_url = $('.fotorama').data('fotorama')['activeFrame']['img']
        image_url = image_url.replace('/uploads/photo/files/', '')
        image_url = image_url.split('/medium_')
        download_url = '/downloads/photo_download?'
        download_url += 'id=' + image_url[0]
        download_url += '&name=' + image_url[1]
        el.attr("href", download_url);
        el.click();
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};
