class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def access_denied(exception)
    if current_admin_user.admin?
      redirect_to admin_root_path
    else
      redirect_to admin_photos_path
    end
  end
end
