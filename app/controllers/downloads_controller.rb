class DownloadsController < ApplicationController
  def photo_download
    if params[:name]
      files = Photo.find(params[:id]).files
      download_file = files.select { |attachment| attachment.path.split("/").last == params[:name] }
      url = download_file.last.file.file
    else
      download_file = Photo.find(params[:id]).files[params[:index].to_i]
      url = download_file.file.file
    end
    send_file url, disposition: 'attachment'
  end
end
