class PhotoAlbumsController < ApplicationController
  respond_to :json, :html

  def index
    @photo_albums = PhotoAlbum.includes(:photos).order(start_date: :asc)
  end
end
