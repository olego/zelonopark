class PhotosController < ApplicationController
  def index
    @photo_album = PhotoAlbum.includes(:photos).find(params[:photo_album_id])
    @current_date = params[:date].present? ? params[:date] : @photo_album.photos.pluck(:date).uniq.first
    if @current_date.present?
      @photos = @photo_album.photos.where(date: @current_date)
    else
      @photos = @photo_album.photos
    end
    @dates = @photo_album.photos.pluck(:date).uniq
  end

  def show
    @photo = Photo.find(params[:id])
  end
end
