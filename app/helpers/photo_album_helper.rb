module PhotoAlbumHelper
  def vk_share_link(photo)
    "http://vkontakte.ru/share.php?url=http://z-fest.ru" +
    "&description=Zelenopark" +
    "&image=http://z-fest.ru#{photo.url(:small)}"
  end

  def fb_share_link(photo)
    "https://www.facebook.com/sharer.php?u=http://z-fest.ru"+
    "&t=Zelenopark" +
    "&picture=http://z-fest.ru#{photo.url(:small)}" +
    "&description=Zelenopark"
  end
end
