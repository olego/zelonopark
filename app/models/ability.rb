class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
      can :manage, PhotoAlbum
    else
      can :manage, Photo
    end
  end

end
