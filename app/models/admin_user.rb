class AdminUser < ActiveRecord::Base
  extend Enumerize

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  enumerize :role, in: [:photograph, :admin], predicates: true

  belongs_to :photo_album
end
