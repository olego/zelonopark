class Photo < ActiveRecord::Base
  include ActiveModel::Validations

  belongs_to :photo_album

  mount_uploaders :files, FileUploader

  validates :date, :photo_album_id, presence: true
  validate :check_files

  def check_files
    if self.files.present?
      res = true
      self.files.map do |file| 
        if file.size >= 15.megabytes
          res = false
          errors.add(:files, "Изображение превышает допустивый лимит в 15мб")
          break
        end
      end
      return res
    else
      errors.add(:files, "Добавьте изображения")
      false
    end
  end
end
