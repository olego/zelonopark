class PhotoAlbum < ActiveRecord::Base
  has_many :photos, dependent: :nullify
  
  has_one :admin_user

  accepts_nested_attributes_for :photos, reject_if: :all_blank, allow_destroy: true

  mount_uploader :logo, LogoUploader
  
  def next
    self.class.where("id > ?", id).first
  end

  def previous
    self.class.where("id < ?", id).last
  end
end
