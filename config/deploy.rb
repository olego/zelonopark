lock '3.5.0'

set :application, 'zelenopark'
set :scm, :git
set :repo_url, 'git@bitbucket.org:olego/zelonopark.git'

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}

set :bundle_binstubs, nil
set :ssh_options, { forward_agent: true, port: 22 }

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end


