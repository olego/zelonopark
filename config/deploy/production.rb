set :stage, :production

set :ssh_options, { forward_agent: true, port: 22 }
set :branch, 'master'
set :deploy_to, '/home/deploy'

server '107.170.123.71', user: 'deploy', roles: %w{web app db}
