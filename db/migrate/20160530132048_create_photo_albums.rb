class CreatePhotoAlbums < ActiveRecord::Migration
  def change
    create_table :photo_albums do |t|
      t.string :title
      t.date :date
      t.string :logo
      t.text :description
      
      t.timestamps null: false
    end
  end
end
