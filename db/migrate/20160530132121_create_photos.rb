class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :attachment
      t.text :description
      t.string :title
      t.integer :photo_album_id

      t.timestamps null: false
    end
  end
end
