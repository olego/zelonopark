class ChangePhotoAlbumColumns < ActiveRecord::Migration
  def change
    rename_column :photo_albums, :date, :start_date
    add_column :photo_albums, :finish_date, :date
  end
end
