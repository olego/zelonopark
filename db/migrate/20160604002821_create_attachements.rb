class CreateAttachements < ActiveRecord::Migration
  def change
    create_table :attachements do |t|
      t.string :file
      t.integer :photo_id
      t.timestamps null: false
    end
  end
end
