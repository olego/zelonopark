class RemoveAttachments < ActiveRecord::Migration
  def change
    remove_column :photos, :attachment
  end
end
