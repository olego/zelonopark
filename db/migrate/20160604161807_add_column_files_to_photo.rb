class AddColumnFilesToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :files, :json
  end
end
