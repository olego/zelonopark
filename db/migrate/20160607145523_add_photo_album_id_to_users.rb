class AddPhotoAlbumIdToUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :photo_album_id, :integer
  end
end
